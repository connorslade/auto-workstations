package uk.co.cablepost.autoworkstations.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.render.RenderLayer;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlockEntityRenderer;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilRegister;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilScreen;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandRegister;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandScreen;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlockEntityRenderer;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableRegister;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableScreen;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableBlockEntityRenderer;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableRegister;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableScreen;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterRegister;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterScreen;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumRegister;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumScreen;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceRegister;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceScreen;

@Environment(EnvType.CLIENT)
public class AutoWorkstationsClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        AutoCraftingTableRegister.onInitializeClient();
        AutoFurnaceRegister.onInitializeClient();
        AutoEnchantingTableRegister.onInitializeClient();
        AutoExperienceOrbVacuumRegister.onInitializeClient();
        AutoExperienceOrbEmitterRegister.onInitializeClient();
        AutoAnvilRegister.onInitializeClient();
        AutoBrewingStandRegister.onInitializeClient();
    }
}