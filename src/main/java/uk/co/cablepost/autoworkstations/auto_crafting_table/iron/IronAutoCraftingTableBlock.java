package uk.co.cablepost.autoworkstations.auto_crafting_table.iron;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableRegister;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class IronAutoCraftingTableBlock extends AutoCraftingTableBlock {
    public IronAutoCraftingTableBlock(Settings settings) {
        super(settings);
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoCraftingTableBlockEntity(pos, state);
    }

    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? validateTicker(type, AutoCraftingTableRegister.IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, IronAutoCraftingTableBlockEntity::clientTick) : validateTicker(type, AutoCraftingTableRegister.IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, IronAutoCraftingTableBlockEntity::serverTick);
    }

    @Override
    public void appendTooltip(ItemStack itemStack, BlockView world, List<Text> tooltip, TooltipContext tooltipContext) {
        tooltip.add( translatableText("block.autoworkstations.iron_auto_crafting_table.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
