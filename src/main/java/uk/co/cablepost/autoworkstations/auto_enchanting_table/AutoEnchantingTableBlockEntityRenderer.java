package uk.co.cablepost.autoworkstations.auto_enchanting_table;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.render.entity.model.BookModel;
import net.minecraft.client.render.entity.model.EntityModelLayers;
import net.minecraft.client.render.model.json.ModelTransformationMode;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RotationAxis;

public class AutoEnchantingTableBlockEntityRenderer<T extends AutoEnchantingTableBlockEntity> implements BlockEntityRenderer<T> {

    public static final SpriteIdentifier BOOK_TEXTURE = new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, new Identifier("entity/enchanting_table_book"));
    private final BookModel book;

    public AutoEnchantingTableBlockEntityRenderer(BlockEntityRendererFactory.Context context) {
        super();
        book = new BookModel(context.getLayerModelPart(EntityModelLayers.BOOK));
    }

    @Override
    public void render(AutoEnchantingTableBlockEntity blockEntity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        matrices.push();

        // --- book start ---

        matrices.push();
        matrices.translate(0.5F, 0.75F, 0.5F);
        float g = (float) blockEntity.ticks + tickDelta;
        matrices.translate(0.0F, 0.1F + MathHelper.sin(g * 0.1F) * 0.01F, 0.0F);

        float h = blockEntity.bookRotation - blockEntity.lastBookRotation;
        while (h >= Math.PI) h -= 2 * Math.PI;
        while (h < -Math.PI) h += 2 * Math.PI;

        float k = blockEntity.lastBookRotation + h * tickDelta;
        matrices.multiply(RotationAxis.POSITIVE_Y.rotation(-k));
        matrices.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(80.0F));
        float l = MathHelper.lerp(tickDelta, blockEntity.pageAngle, blockEntity.nextPageAngle);
        float m = MathHelper.fractionalPart(l + 0.25F) * 1.6F - 0.3F;
        float n = MathHelper.fractionalPart(l + 0.75F) * 1.6F - 0.3F;
        float o = MathHelper.lerp(tickDelta, blockEntity.pageTurningSpeed, blockEntity.nextPageTurningSpeed);
        this.book.setPageAngles(g, MathHelper.clamp(m, 0.0F, 1.0F), MathHelper.clamp(n, 0.0F, 1.0F), o);
        VertexConsumer vertexConsumer = BOOK_TEXTURE.getVertexConsumer(vertexConsumers, RenderLayer::getEntitySolid);
        this.book.renderBook(matrices, vertexConsumer, light, overlay, 1.0F, 1.0F, 1.0F, 1.0F);
        matrices.pop();

        // ---  book end  ---

        if (blockEntity.expLevel > 0) {
            // --- xp inside start ---
            ItemStack xpInside = new ItemStack(AutoEnchantingTableRegister.AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK);
            xpInside.addEnchantment(Enchantment.byRawId(0), 1);

            float xpInsideUpAmt = 0.55f + (0.45f * Math.min(blockEntity.expLevel / 30f, 1f));//0.55 - 1.0


            matrices.translate(0.5, 0.5f, 0.5f);
            matrices.scale(0.9f, 0.9f * xpInsideUpAmt, 0.9f);
            MinecraftClient.getInstance().getItemRenderer()
                    .renderItem(xpInside, ModelTransformationMode.GROUND, light, overlay, matrices, vertexConsumers, blockEntity.getWorld(), 0);
            matrices.scale(1f / 0.9f, 1f / (0.9f * xpInsideUpAmt), 1f / 0.9f);
            matrices.translate(-0.5f, -0.5f, -0.5f);
            // ---  xp inside end  ---

            // --- green floor inside start ---
            ItemStack greenFloor = new ItemStack(Items.LIME_CONCRETE);

            matrices.translate(0.5, 0.67f, 0.5f);
            matrices.scale(0.97f, 0.1f, 0.97f);
            MinecraftClient.getInstance().getItemRenderer()
                    .renderItem(greenFloor, ModelTransformationMode.GROUND, 100, overlay, matrices, vertexConsumers, blockEntity.getWorld(), 0);
            matrices.scale(1f / 0.97f, 10f, 1f / 0.97f);
            matrices.translate(-0.5f, -0.67f, -0.5f);
            // ---  green floor inside end  ---
        }

        matrices.pop();
    }
}
