package uk.co.cablepost.autoworkstations.util;

import net.minecraft.text.MutableText;

import net.minecraft.text.Text;

public class TextUtil {
    public static MutableText translatableText(String txtKey){
        return Text.translatable(txtKey);
    }

    public static MutableText literalText(String txt){
        return Text.literal(txt);
    }
}
