package uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.iron;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterRegister;

public class IronAutoExperienceOrbEmitterBlockEntity extends AutoExperienceOrbEmitterBlockEntity {
    public IronAutoExperienceOrbEmitterBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoExperienceOrbEmitterRegister.IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY, blockPos, blockState);
        xpEmptyBottleMaxProgress = 20;
        xpEmitTimerMax = 4;
    }
}
